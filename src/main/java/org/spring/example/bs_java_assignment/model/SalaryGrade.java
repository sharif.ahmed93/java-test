package org.spring.example.bs_java_assignment.model;

import javax.persistence.*;

@Entity
@Table(name = "tbl_salary_grade")
public class SalaryGrade {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int gradeId;
    private String gradeName;
    private Double gradeSalary;

    public void setGradeId(int id) {
        this.gradeId = id;
    }


    public int getGradeId() {
        return gradeId;
    }

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }

    public Double getGradeSalary() {
        return gradeSalary;
    }

    public void setGradeSalary(Double gradeSalary) {
        this.gradeSalary = gradeSalary;
    }
}
