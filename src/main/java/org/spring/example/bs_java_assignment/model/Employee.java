package org.spring.example.bs_java_assignment.model;


import javax.persistence.*;

@Entity
@Table(name = "tbl_employee")
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int empId;
    private String name;
    private String address;
    private String mobile;
    @OneToOne
    @JoinColumn(name = "grade_Id")
    private SalaryGrade grade;
    @OneToOne
    @JoinColumn(name = "bank_Id")
    private BankAccount bankAccount;

    public void setEmpId(int id) {
        this.empId = id;
    }

    public int getEmpId() {
        return empId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public SalaryGrade getGrade() {
        return grade;
    }

    public void setGrade(SalaryGrade grade) {
        this.grade = grade;
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }
}
