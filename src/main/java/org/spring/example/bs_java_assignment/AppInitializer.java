package org.spring.example.bs_java_assignment;


import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;

public class AppInitializer implements WebApplicationInitializer {

    public void onStartup(ServletContext servletCxt) {

        // Load Spring web application configuration
        AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();
        rootContext.register(org.spring.example.bs_java_assignment.RootConfig.class);

        rootContext.getEnvironment().setActiveProfiles("dev");
        //rootContext.getEnvironment().setActiveProfiles("prod");

        rootContext.refresh();

        servletCxt.addListener(new ContextLoaderListener(rootContext));

        // Create and register the DispatcherServlet
        AnnotationConfigWebApplicationContext servletRegisterer = new AnnotationConfigWebApplicationContext();
        servletRegisterer.register(org.spring.example.bs_java_assignment.ServletConfig.class);
        ServletRegistration.Dynamic registration = servletCxt.addServlet("servlet", new DispatcherServlet(servletRegisterer));
        registration.setLoadOnStartup(1);
        registration.addMapping("/");

    }
}
