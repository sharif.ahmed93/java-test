package org.spring.example.bs_java_assignment.config;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.reflections.Reflections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Modifier;
import java.util.Properties;

public class HibernateConfig {

    @Autowired
    private Environment environment;

    private SessionFactory sessionFactory = null;

    private Session session;

    public Session getSession() {
        try {
            this.session = createAndGetLocalSessionFactoryBean().getCurrentSession();
        } catch (HibernateException ex) {
            ex.printStackTrace();
            System.out.println("Opening new session...");
            this.session = createAndGetLocalSessionFactoryBean().openSession();
        }
        return this.session;
    }

    public CriteriaBuilder getCriteriaBuilder() {
        Session session = getSession();
        Transaction tx = session.getTransaction();
        if (!tx.isActive()) {
            tx = session.beginTransaction();
        }
        return session.getCriteriaBuilder();
    }

    public EntityManager entityManager() {
        return createAndGetLocalSessionFactoryBean().createEntityManager();
    }

    public SessionFactory createAndGetLocalSessionFactoryBean() {
        if (this.sessionFactory == null) {
            try {
                Configuration configuration = new Configuration();
                // Hibernate settings equivalent to hibernate.cfg.xml's properties
                //Properties settings = getBuiltProperties("hibernate.properties");
                Properties settings = getBuiltProperties("hibernate-" + environment.getActiveProfiles()[0] + ".properties");

                configuration.setProperties(settings);
                configuration.addPackage("org.spring.example.bs_java_assignment.model");
                for (Class<?> clazz : (new Reflections("org.spring.example.bs_java_assignment.model")).getTypesAnnotatedWith(Entity.class)) {
                    if (!Modifier.isAbstract(clazz.getModifiers())) {
                        configuration.addAnnotatedClass(clazz);
                    }
                }
                StandardServiceRegistryBuilder serviceRegistry = new StandardServiceRegistryBuilder()
                        .applySettings(settings);
                sessionFactory = configuration.buildSessionFactory(serviceRegistry.build());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return sessionFactory;
    }

    protected Properties getBuiltProperties(String propertyFileName) {
        Properties properties = new Properties();
        InputStream input = HibernateConfig.class
                .getClassLoader().getResourceAsStream(propertyFileName);
        try {
            properties.load(input);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties;
    }
}
