package org.spring.example.bs_java_assignment.dto;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

public class GradeDto implements Serializable {
    //@NotEmpty
    private String gradeName;
    //@NotEmpty
    private Double gradeSalary;

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }

    public Double getGradeSalary() {
        return gradeSalary;
    }

    public void setGradeSalary(Double gradeSalary) {
        this.gradeSalary = gradeSalary;
    }
}
