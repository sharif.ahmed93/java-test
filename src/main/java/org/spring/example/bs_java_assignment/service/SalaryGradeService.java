package org.spring.example.bs_java_assignment.service;

import java.util.List;
import org.spring.example.bs_java_assignment.model.*;

public interface SalaryGradeService {
    public void addSalaryGrade(SalaryGrade grade);

    public List<SalaryGrade> getAllGrades();

    public List<SalaryGrade> getAllGradesByGradeId(long gradeId);
}
