package org.spring.example.bs_java_assignment.service;

import org.spring.example.bs_java_assignment.dao.GradeDao;
import org.spring.example.bs_java_assignment.model.SalaryGrade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SalaryGradeServiceImpl implements SalaryGradeService {

    @Autowired
    GradeDao gradeDao;

    @Override
    public void addSalaryGrade(SalaryGrade grade) {
        gradeDao.addSalaryGrade(grade);
    }

    @Override
    public List<SalaryGrade> getAllGrades() {
        return gradeDao.getAllGrades();
    }

    @Override
    public List<SalaryGrade> getAllGradesByGradeId(long gradeId) {
        return gradeDao.getAllGradesByGradeId(gradeId);
    }
}
