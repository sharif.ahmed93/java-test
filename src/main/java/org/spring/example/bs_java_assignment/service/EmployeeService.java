package org.spring.example.bs_java_assignment.service;

import org.spring.example.bs_java_assignment.model.Employee;

import java.util.List;

public interface EmployeeService {
    public void addEmployee(Employee employee);
    public List<Employee> getAllEmploys();
    public Employee getEmployeeById(long empId);
}
