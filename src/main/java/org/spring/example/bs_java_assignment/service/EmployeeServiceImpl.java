package org.spring.example.bs_java_assignment.service;

import org.spring.example.bs_java_assignment.dao.BankAccountDao;
import org.spring.example.bs_java_assignment.dao.EmployeeDao;
import org.spring.example.bs_java_assignment.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    EmployeeDao employeeDao;

    @Override
    public void addEmployee(Employee employee) {
        employeeDao.addEmployee(employee);
    }

    @Override
    public List<Employee> getAllEmploys() {
        return  employeeDao.getAllEmploys();
    }

    @Override
    public Employee getEmployeeById(long empId) {
        return employeeDao.getEmployeeById(empId);
    }
}
