package org.spring.example.bs_java_assignment.service;

import org.spring.example.bs_java_assignment.dao.BankAccountDao;
import org.spring.example.bs_java_assignment.dao.GradeDao;
import org.spring.example.bs_java_assignment.model.BankAccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BankAccountServiceImpl implements BankAccountService {

    @Autowired
    BankAccountDao bankAccountDao;

    @Override
    public void addBankAccount(BankAccount bankAccount) {
        bankAccountDao.addBankAccount(bankAccount);
    }

    @Override
    public List<BankAccount> getAllBankAccounts() {
        return bankAccountDao.getAllBankAccounts();
    }

    @Override
    public BankAccount getBankAccountsById(long bankAccountId) {
        return bankAccountDao.getBankAccountsById(bankAccountId);
    }

    @Override
    public boolean isBankAccountExists(String accountNumber) {
        return bankAccountDao.isBankAccountExists(accountNumber);
    }
}
