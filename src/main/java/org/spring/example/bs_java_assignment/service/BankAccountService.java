package org.spring.example.bs_java_assignment.service;

import org.spring.example.bs_java_assignment.model.BankAccount;

import java.util.List;

public interface BankAccountService {

    public void addBankAccount(BankAccount bankAccount);

    public List<BankAccount> getAllBankAccounts();

    public BankAccount getBankAccountsById(long bankAccountId);

    public boolean isBankAccountExists(String accountNumber);
}
