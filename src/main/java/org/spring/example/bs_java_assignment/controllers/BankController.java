package org.spring.example.bs_java_assignment.controllers;

import org.spring.example.bs_java_assignment.dto.BankAccountDto;
import org.spring.example.bs_java_assignment.dto.GradeDto;
import org.spring.example.bs_java_assignment.model.BankAccount;
import org.spring.example.bs_java_assignment.model.SalaryGrade;
import org.spring.example.bs_java_assignment.service.BankAccountService;
import org.spring.example.bs_java_assignment.service.SalaryGradeService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
public class BankController {


    @Autowired
    BankAccountService bankAccountService;

    @PostMapping("/addBankAccount")
    public String addBank(Model model, @ModelAttribute("bankAccountDto") @Valid BankAccountDto bankAccountDto, BindingResult bindingResult) {

        if (!bindingResult.hasErrors()) {
            if (!bankAccountService.isBankAccountExists(bankAccountDto.getAccountNumber())) {
                BankAccount account = new BankAccount();

                BeanUtils.copyProperties(bankAccountDto, account);

                bankAccountService.addBankAccount(account);

                model.addAttribute("message", "Bank Account added successfully done!");
                return "redirect:/bank-account-list";
            }
            model.addAttribute("message", "Same Bank Account Alreadey Added!");
            return "add-bank-account";
        }
        return "add-bank-account";
    }

    @GetMapping("/bank-account-list")
    public String getBankAccountList(Model model) {
        List<BankAccount> bankAccountList = bankAccountService.getAllBankAccounts();
        model.addAttribute("bankAccountList", bankAccountList);
        return "bank-account-list";
    }
}
