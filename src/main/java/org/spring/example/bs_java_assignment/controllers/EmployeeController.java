package org.spring.example.bs_java_assignment.controllers;


import org.spring.example.bs_java_assignment.dto.BankAccountDto;
import org.spring.example.bs_java_assignment.dto.EmployeeDto;
import org.spring.example.bs_java_assignment.model.BankAccount;
import org.spring.example.bs_java_assignment.model.Employee;
import org.spring.example.bs_java_assignment.model.SalaryGrade;
import org.spring.example.bs_java_assignment.service.BankAccountService;
import org.spring.example.bs_java_assignment.service.EmployeeService;
import org.spring.example.bs_java_assignment.service.SalaryGradeService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
public class EmployeeController {


    @Autowired
    SalaryGradeService salaryGradeService;

    @Autowired
    BankAccountService bankAccountService;

    @Autowired
    EmployeeService employeeService;

    @PostMapping("/addEmployee")
    public String addEmployee(Model model, @ModelAttribute("employeeDto") @Valid EmployeeDto employeeDto, BindingResult bindingResult){

        if (!bindingResult.hasErrors()) {

            Employee employee = new Employee();

            BeanUtils.copyProperties(employeeDto, employee);

            employeeService.addEmployee(employee);

            model.addAttribute("message", "Employee added successfully done!");
            return "redirect:/employee-list";
        }
        return "add-employee";
    }

    @GetMapping("/employee-list")
    public String getEmployeeList(Model model){

        List<Employee> employeeList = employeeService.getAllEmploys();
        List<BankAccount> accountList = bankAccountService.getAllBankAccounts();
        List<SalaryGrade> gradeList = salaryGradeService.getAllGrades();

        model.addAttribute("employeeList", employeeList);
        model.addAttribute("accountList", accountList);
        model.addAttribute("gradeList", gradeList);
        return "employee-list";
    }

}
