package org.spring.example.bs_java_assignment.controllers;

import org.spring.example.bs_java_assignment.service.SalaryGradeService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.spring.example.bs_java_assignment.dto.*;
import org.spring.example.bs_java_assignment.model.*;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
public class AddGradeController {


    @Autowired
    SalaryGradeService salaryGradeService;



    @PostMapping("/addGrade")
    public String addLowestGrade(Model model, @ModelAttribute("gradeDto") @Valid GradeDto gradeDto, BindingResult bindingResult){

        if (!bindingResult.hasErrors()) {
            List<SalaryGrade> gradeList = salaryGradeService.getAllGrades();
            if (gradeList.isEmpty()){
                SalaryGrade salaryGrade_6 = new SalaryGrade();
                SalaryGrade salaryGrade_5 = new SalaryGrade();
                SalaryGrade salaryGrade_4 = new SalaryGrade();
                SalaryGrade salaryGrade_3 = new SalaryGrade();
                SalaryGrade salaryGrade_2 = new SalaryGrade();
                SalaryGrade salaryGrade_1 = new SalaryGrade();

                BeanUtils.copyProperties(gradeDto, salaryGrade_6);

                double grade_6_Salary = salaryGrade_6.getGradeSalary();
                salaryGradeService.addSalaryGrade(salaryGrade_6);

                double grade_5_Salary = grade_6_Salary+5000.0;
                salaryGrade_5.setGradeName("Grade_5");
                salaryGrade_5.setGradeSalary(grade_5_Salary);
                salaryGradeService.addSalaryGrade(salaryGrade_5);

                double grade_4_Salary = grade_5_Salary+5000.0;
                salaryGrade_4.setGradeName("Grade_4");
                salaryGrade_4.setGradeSalary(grade_4_Salary);
                salaryGradeService.addSalaryGrade(salaryGrade_4);

                double grade_3_Salary = grade_4_Salary+5000.0;
                salaryGrade_3.setGradeName("Grade_3");
                salaryGrade_3.setGradeSalary(grade_3_Salary);
                salaryGradeService.addSalaryGrade(salaryGrade_3);

                double grade_2_Salary = grade_3_Salary+5000.0;
                salaryGrade_2.setGradeName("Grade_2");
                salaryGrade_2.setGradeSalary(grade_2_Salary);
                salaryGradeService.addSalaryGrade(salaryGrade_2);

                double grade_1_Salary = grade_2_Salary+5000.0;
                salaryGrade_1.setGradeName("Grade_1");
                salaryGrade_1.setGradeSalary(grade_1_Salary);
                salaryGradeService.addSalaryGrade(salaryGrade_1);

                model.addAttribute("message", "Lowest Grade Salary added successfully done!");
                return "redirect:/grade-list";
            }
            model.addAttribute("message", "Already Lowest Grade Salary Added");
            return "redirect:/add-grade";
        }
        return "add-grade";
    }

    @GetMapping("/grade-list")
    public String getSalaryGradeList(Model model){
        List<SalaryGrade> gradeList = salaryGradeService.getAllGrades();
        model.addAttribute("gradeList", gradeList);
        return "grade-list";
    }
}
