package org.spring.example.bs_java_assignment.controllers;

import org.spring.example.bs_java_assignment.model.BankAccount;
import org.spring.example.bs_java_assignment.service.BankAccountService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class RootController {

    @Autowired
    BankAccountService bankAccountService;

    @GetMapping("/")
    public String rootMap(Model model) {
        return "redirect:/index";
    }

    @GetMapping("/index")
    public String userHomePage(Model model) {
        if (!bankAccountService.isBankAccountExists("1234500003330")){
            BankAccount account = new BankAccount();
            account.setAccountName("BS-23");
            account.setAccountNumber("1234500003330");
            account.setAccountType("CURRENT");
            account.setCurrentBalance(150000.0);
            account.setBankName("Brac Bank");
            account.setBranchName("Mohakhali");
            bankAccountService.addBankAccount(account);
        }
        return "index";
    }
}
