package org.spring.example.bs_java_assignment.controllers;


import org.spring.example.bs_java_assignment.dto.BankAccountDto;
import org.spring.example.bs_java_assignment.dto.EmployeeDto;
import org.spring.example.bs_java_assignment.dto.GradeDto;
import org.spring.example.bs_java_assignment.model.BankAccount;
import org.spring.example.bs_java_assignment.model.SalaryGrade;
import org.spring.example.bs_java_assignment.service.BankAccountService;
import org.spring.example.bs_java_assignment.service.SalaryGradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class HomeController {


    @Autowired
    SalaryGradeService salaryGradeService;

    @Autowired
    BankAccountService bankAccountService;

    @GetMapping("/add-grade")
    public String getAddGradeView(Model model){
        model.addAttribute("gradeDto", new GradeDto());
        return "add-grade";
    }

    @GetMapping("/add-bank-account")
    public String getAddBankView(Model model){
        model.addAttribute("bankAccountDto", new BankAccountDto());
        return "add-bank-account";
    }


    @GetMapping("/add-employee")
    public String getAddEmployeeView(Model model){
        List<BankAccount> accountList = bankAccountService.getAllBankAccounts();
        List<SalaryGrade> gradeList = salaryGradeService.getAllGrades();
        model.addAttribute("employeeDto", new EmployeeDto());
        model.addAttribute("bankAccountList", accountList);
        model.addAttribute("salaryGradeList", gradeList);
        return "add-employee";
    }
}
