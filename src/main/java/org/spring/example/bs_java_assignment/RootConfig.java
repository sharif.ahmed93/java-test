package org.spring.example.bs_java_assignment;

import org.spring.example.bs_java_assignment.config.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;


@ComponentScan(basePackages = {
        "org.spring.example.bs_java_assignment.config",
        "org.spring.example.bs_java_assignment.service",
        "org.spring.example.bs_java_assignment.dao"
})
public class RootConfig {
    @Bean
    GlobalExceptionHandler globalExceptionHandler() {
        return new GlobalExceptionHandler();
    }

    @Bean
    HibernateConfig hibernateConfig() {
        return new HibernateConfig();
    }

}
