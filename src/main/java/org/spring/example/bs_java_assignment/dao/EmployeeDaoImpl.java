package org.spring.example.bs_java_assignment.dao;


import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.spring.example.bs_java_assignment.config.HibernateConfig;
import org.spring.example.bs_java_assignment.model.BankAccount;
import org.spring.example.bs_java_assignment.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class EmployeeDaoImpl implements EmployeeDao {

    @Autowired
    HibernateConfig hibernateConfig;

    @Override
    public void addEmployee(Employee employee) {

        Session session = hibernateConfig.getSession();
        Transaction tx = session.getTransaction();

        if (!tx.isActive()) {
            tx = session.beginTransaction();
        }

        try {
            session.save(employee);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();

        } finally {
            session.close();
        }
    }

    @Override
    public List<Employee> getAllEmploys() {
        Session session = hibernateConfig.getSession();
        Transaction tx = session.getTransaction();
        if (!tx.isActive()){
            tx = session.beginTransaction();
        }
        List<Employee> employees = session.createQuery("from Employee ").list();
        tx.commit();
        return employees;
    }

    @Override
    public Employee getEmployeeById(long empId) {
        Session session = hibernateConfig.getSession();
        Transaction tx = session.getTransaction();

        if (!tx.isActive()) {
            tx = session.beginTransaction();
        }

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Employee> userCriteriaQuery = cb.createQuery(Employee.class);
        Root<Employee> root = userCriteriaQuery.from(Employee.class);

        userCriteriaQuery.where(cb.equal(root.get("empId"), empId));

        Employee emp = new Employee();
        try {
            emp = session.createQuery(userCriteriaQuery).getSingleResult();
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }

        return emp;
    }
}
