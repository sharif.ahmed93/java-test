package org.spring.example.bs_java_assignment.dao;

import org.spring.example.bs_java_assignment.model.BankAccount;
import org.spring.example.bs_java_assignment.model.SalaryGrade;

import java.util.List;

public interface BankAccountDao {

    public void addBankAccount(BankAccount bankAccount);

    public List<BankAccount> getAllBankAccounts();

    public BankAccount getBankAccountsById(long bankAccountId);

    public boolean isBankAccountExists(String accountNumber);
}
