package org.spring.example.bs_java_assignment.dao;


import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.spring.example.bs_java_assignment.config.HibernateConfig;
import org.spring.example.bs_java_assignment.model.BankAccount;
import org.spring.example.bs_java_assignment.model.SalaryGrade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Repository
public class BankAccountDaoImpl implements BankAccountDao {

    @Autowired
    HibernateConfig hibernateConfig;


    @Override
    public void addBankAccount(BankAccount bankAccount) {
        Session session = hibernateConfig.getSession();
        Transaction tx = session.getTransaction();

        if (!tx.isActive()) {
            tx = session.beginTransaction();
        }

        try {
            session.save(bankAccount);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();

        } finally {
            session.close();
        }
    }

    @Override
    public List<BankAccount> getAllBankAccounts() {
        Session session = hibernateConfig.getSession();
        Transaction tx = session.getTransaction();
        if (!tx.isActive()){
            tx = session.beginTransaction();
        }
        List<BankAccount> bankAccounts = session.createQuery("from BankAccount ").list();
        tx.commit();
        return bankAccounts;
    }

    @Override
    public BankAccount getBankAccountsById(long bankAccountId) {
        Session session = hibernateConfig.getSession();
        Transaction tx = session.getTransaction();

        if (!tx.isActive()) {
            tx = session.beginTransaction();
        }

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<BankAccount> userCriteriaQuery = cb.createQuery(BankAccount.class);
        Root<BankAccount> root = userCriteriaQuery.from(BankAccount.class);

        userCriteriaQuery.where(cb.equal(root.get("accId"), bankAccountId));

        BankAccount account = new BankAccount();
        try {
            account = session.createQuery(userCriteriaQuery).getSingleResult();
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }

        return account;
    }

    @Override
    public boolean isBankAccountExists(String accountNumber) {

        Session session = hibernateConfig.getSession();
        Transaction tx = session.getTransaction();

        if (!tx.isActive()) tx = session.beginTransaction();

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<BankAccount> userCriteriaQuery = cb.createQuery(BankAccount.class);
        Root<BankAccount> root = userCriteriaQuery.from(BankAccount.class);

        userCriteriaQuery.where(cb.equal(root.get("accountNumber"), accountNumber));

        ArrayList<BankAccount> userList = new ArrayList<BankAccount>();
        try {
            userList = (ArrayList<BankAccount>) session.createQuery(userCriteriaQuery).getResultList();
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }


        return userList.size() > 0 ? true : false;
    }
}
