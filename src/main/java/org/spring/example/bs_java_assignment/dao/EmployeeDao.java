package org.spring.example.bs_java_assignment.dao;

import org.spring.example.bs_java_assignment.model.BankAccount;
import org.spring.example.bs_java_assignment.model.Employee;

import java.util.List;

public interface EmployeeDao {
    public void addEmployee(Employee employee);
    public List<Employee> getAllEmploys();
    public Employee getEmployeeById(long empId);
}
