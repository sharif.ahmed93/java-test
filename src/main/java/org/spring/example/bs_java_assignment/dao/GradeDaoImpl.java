package org.spring.example.bs_java_assignment.dao;

import lombok.var;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.spring.example.bs_java_assignment.model.*;
import org.spring.example.bs_java_assignment.config.*;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class GradeDaoImpl implements GradeDao {


    @Autowired
    HibernateConfig hibernateConfig;

    @Override
    public void addSalaryGrade(SalaryGrade grade) {
        Session session = hibernateConfig.getSession();
        Transaction tx = session.getTransaction();

        if (!tx.isActive()) {
            tx = session.beginTransaction();
        }

        try {
            session.save(grade);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();

        } finally {
            session.close();
        }
    }

    @Override
    public List<SalaryGrade> getAllGrades() {
        Session session = hibernateConfig.getSession();
        Transaction tx = session.getTransaction();
        if (!tx.isActive()){
            tx = session.beginTransaction();
        }
        List<SalaryGrade> salaryGrades = session.createQuery("from SalaryGrade ").list();
        tx.commit();
        return salaryGrades;
    }

    @Override
    public List<SalaryGrade> getAllGradesByGradeId(long gradeId) {
        Session session = hibernateConfig.getSession();
        Transaction tx = session.getTransaction();
        if (!tx.isActive()) {
            tx = session.beginTransaction();
        }

        TypedQuery<SalaryGrade> query = session.getEntityManagerFactory().createEntityManager()
                .createQuery("select c from SalaryGrade c where c.gradeId=:gradeId ", SalaryGrade.class);
        query.setParameter("gradeId", gradeId);
        List<SalaryGrade> salaryGrades = query.getResultList();

        tx.commit();
        return salaryGrades;
    }
}
