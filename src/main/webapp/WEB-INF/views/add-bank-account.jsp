<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: Sharif
  Date: 9/25/2020
  Time: 10:32 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Add Bank</title>
</head>
<body>
<div class="container">
    <%--@elvariable id="bankAccountDto" type="org.spring.example.bs_java_assignment.dto.BankAccountDto"--%>
    <form:form action="${pageContext.request.contextPath}/addBankAccount" method="post" modelAttribute="bankAccountDto">
        <div class="form-group">
            <label for="name">Account Name:</label>
            <form:input class="form-control" name="accountName" path="accountName" id="name" required="required" type="text"/>
        </div>
        <div class="form-group">
            <label for="amount">Account Number:</label>
            <form:input class="form-control" name="accountNumber" path="accountNumber" id="amount" required="required" type="text"/>
        </div>

        <div class="form-group">
            <label for="amount">Account Type:</label>
            <form:input class="form-control" name="accountType" path="accountType" id="amount" required="required" type="text"/>
        </div>

        <div class="form-group">
            <label for="amount">Current Balance:</label>
            <form:input class="form-control" name="currentBalance" path="currentBalance" id="amount" required="required" type="text"/>
        </div>

        <div class="form-group">
            <label for="amount">Bank Name:</label>
            <form:input class="form-control" name="bankName" path="bankName" id="amount" required="required" type="text"/>
        </div>

        <div class="form-group">
            <label for="amount">Branch Name:</label>
            <form:input class="form-control" name="branchName" path="branchName" id="amount" required="required" type="text"/>
        </div>

        <button type="submit" class="btn btn-default">Submit</button>
    </form:form>
</div>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
</body>
</html>
