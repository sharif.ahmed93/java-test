<%--
  Created by IntelliJ IDEA.
  User: Sharif
  Date: 9/26/2020
  Time: 7:32 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core_1_1" %>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Bank Account List</title>
</head>
<body>
<div class="container">
    <%--@elvariable id="gradeDto" type="org.spring.example.bs_java_assignment.dto.GradeDto"--%>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Account Name</th>
            <th scope="col">Account Type</th>
            <th scope="col">Account Number</th>
            <th scope="col">Current Balance</th>
            <th scope="col">Bank Name</th>
            <th scope="col">Branch Name</th>
        </tr>
        </thead>
        <tbody>
        <%--@elvariable id="bankAccountList" type="java.util.List"--%>
        <c:forEach items="${bankAccountList}" var="bankAccount">
            <tr>
                <th scope="row">${bankAccount.accId}</th>
                <td>${bankAccount.accountName}</td>
                <td>${bankAccount.accountType}</td>
                <td>${bankAccount.accountNumber}</td>
                <td>${bankAccount.currentBalance}</td>
                <td>${bankAccount.bankName}</td>
                <td>${bankAccount.branchName}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
</body>
</html>
